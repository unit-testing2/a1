const { assert } = require('chai');
const { newUser } = require('../index.js');

// describe(name, function)
// will handle case. 
describe("Test newUser object",() => {
	// it(name, function)
	// this will specify the different test case.
	it("Assert newUser type is an object",() => {
		assert.equal(typeof(newUser),'object');
	});
	it("Assert newUser email is type string", () => {
		assert.equal(typeof(newUser.email),'string');
	});
	it("Assert newUser email is not defined", () => {
		assert.notEqual(typeof(newUser.email, 'undefined'));
	});
	it("Assert newUser password is type string", () => {
		assert.equal(typeof(newUser.password),'string');
	});
	it("Assert newUser password has at least 16 characters", () => {
		assert.isAtLeast(newUser.password.length, 16);
	});

	it("Assert newUser firstname type is a string", () => {	
		assert.equal(typeof(newUser.firstname), 'string');
	});
	it("Assert newUser lastname type is a string", () => {	
		assert.equal(typeof(newUser.lastname), 'string');

	});
	it("Assert newUser firstname type is not defined", () => {	
		assert.notEqual(typeof(newUser.firstname), 'undefined');

	});
	it("Assert newUser lastname type is not defined", () => {	
		assert.notEqual(typeof(newUser.lastname), 'undefined');
	});
	it("Assert newUser age is at least 18", () => {
		assert.isAtLeast(newUser.age, 18);
	});
	it("Assert newUser age is a number", () => {	
		assert.equal(typeof(newUser.age), 'number');
	});
	it("Assert newUser contact number type is a string", () => {	
		assert.equal(typeof(newUser.contactNo), 'string');
	});
	it("Assert newUser batch number type is a number", () => {	
		assert.equal(typeof(newUser.batchNo), 'number');
	});
	it("Assert newUser batch number type is not undefined", () => {	
		assert.notEqual(typeof(newUser.batchNo), 'undefined');

	});
});
