const express = require('express');
const app = express();
const port = 5000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

const newUser = {
	firstname: "james",
	lastname: "villanueva",
	age: 24,
	contactNo: "09120403489",
	batchNo: 166,
	email: "jamesvillanueva@gmail.com",
	password: "1234567890123456"
};

module.exports = {
	newUser: newUser
};

app.listen(port, () => {console.log(`Server is running at port ${port}.`)});